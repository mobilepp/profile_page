import 'package:flutter/material.dart';

enum APP_THEME{LIGHT, DARK}

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.white,
            iconTheme: IconThemeData(
              color: Colors.black,
            )
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade300,
        )
    );
  }

  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.white,
            iconTheme: IconThemeData(
              color: Colors.indigo.shade900,
            )
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade300,
        )
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        theme: currentTheme == APP_THEME.DARK
            ? MyAppTheme.appThemeLight()
            : MyAppTheme.appThemeDark(),
        home: Scaffold(
          appBar: buildAppBarWidget(),
          body: buildBodyWidget(),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              setState(() {
                currentTheme == APP_THEME.DARK
                    ? currentTheme = APP_THEME.LIGHT
                    : currentTheme = APP_THEME.DARK;
              });
              },
            child: currentTheme == APP_THEME.DARK
                ? Icon(Icons.light_mode)
                : Icon(Icons.dark_mode),
            // IconButton(
            //   icon : Icon(Icons.toggle_on),
            //   onPressed: () {
            //     setState(() {
            //       currentTheme == APP_THEME.DARK
            //           ? currentTheme == APP_THEME.LIGHT
            //           : currentTheme == APP_THEME.DARK;
            //     });
            //   },
            // ),
          ),
      ),
    );
  }

  AppBar buildAppBarWidget() {
    return AppBar(
      backgroundColor: Colors.indigo[200], //backgroundColor: Colors.cyan[200],
      leading: Icon(
        Icons.arrow_back,
        // color: Colors.black,
      ),
      // title: Text("Hello Flutter App"),
      actions: <Widget>[
        IconButton(
            onPressed: (){},
            icon: Icon(
              Icons.star_border_outlined,
              // color: Colors.black,
            )
        ),
      ],
    );
  }

  Widget buildBodyWidget(){
    return ListView (
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              // color: Colors.cyan,
              width: double.infinity,
              //Height constraint at Container widget level
              height: 250,
              child: Image.network(
                "https://f.ptcdn.info/133/073/000/qresfm40lwb2M7h1ba0c-o.jpg",
                fit: BoxFit.cover,
              ),//"https://scontent.fbkk10-1.fna.fbcdn.net/v/t39.30808-6/294150939_3169601319968777_1167878594474576026_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeEQshV6q57WIh_WTfVQcB_eRzWxwHOjPKhHNbHAc6M8qOD5wQY7JtqyXT5H4CGwp_16tJYSZPkY68d4IXu0BlRr&_nc_ohc=Iit7qwE_28AAX-J4rzt&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfBnD-Q9MTMpA9UetjOzqhkLN-LjfE9E0zV7AC19Mwe42g&oe=63A74EC7",
            ),
            Container(
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(8.0),
                    child: Text("Jutamas Lueariyasap",
                      style: TextStyle(fontSize: 30), //"Ishigami Senku"
                    ),
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.blueGrey[600],
            ),
            Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child : Theme(
                data: ThemeData (
                  iconTheme: IconThemeData(
                    color: Colors.indigo.shade300,
                  )
                ),
                child: profileActionItems(),
              )
            ),
            Divider(
              color: Colors.blueGrey[600],
            ),
            mobilePhoneListTile(),
            otherPhoneListTile(),
            Divider(
              color: Colors.blueGrey[600],
            ),
            emailListTile(),
            Divider(
              color: Colors.blueGrey[600],
            ),
            addressListTile(),
          ],
        ),
      ],
    );
  }

  Widget profileActionItems(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildCallButton(),
        buildTextButton(),
        buildVideoButton(),
        buildEmailButton(),
        buildDirectionButton(),
        buildPayButton(),
      ],
    );
  }

  Widget buildCallButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.call,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Call"),
      ],
    );
  }

  Widget buildTextButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.message,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Text"),
      ],
    );
  }

  Widget buildVideoButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.video_call,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Video"),
      ],
    );
  }

  Widget buildEmailButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.email,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Email"),
      ],
    );
  }

  Widget buildDirectionButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.directions,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Direction"),
      ],
    );
  }

  Widget buildPayButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.attach_money,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Pay"),
      ],
    );
  }

  Widget mobilePhoneListTile(){
    return ListTile(
          leading: Icon(Icons.call),
          title: Text('062-531-9822'),
          subtitle: Text('mobile'),
          trailing: IconButton (
              icon: Icon(Icons.message),
              // color: Colors.indigo.shade800,
              onPressed: () {},
          ),
    );
  }

  Widget otherPhoneListTile(){
    return ListTile(
          leading: Text(""),
          title: Text('850-508-3390'),
          subtitle: Text('other'),
          trailing: IconButton (
            icon: Icon(Icons.message),
            // color: Colors.indigo.shade800,
            onPressed: () {},
          ),
    );
  }

  Widget emailListTile(){
    return ListTile(
          leading: Icon(Icons.email),
          title: Text('63160066@go.buu.ac.th'),
          subtitle: Text('work'),
        );
  }

  Widget addressListTile(){
    return ListTile(
          leading: Icon(Icons.location_on),
          title: Text('1285 Sunset st. StoneWorld'),
          subtitle: Text('home'),
          trailing: IconButton (
            icon: Icon(Icons.directions),
            // color: Colors.indigo.shade800,
            onPressed: () {},
          ),
    );
  }
}